!macro customInstall
  WriteRegStr SHCTX "SOFTWARE\RegisteredApplications" "Hercules" "Software\Clients\StartMenuInternet\Hercules\Capabilities"

  WriteRegStr SHCTX "SOFTWARE\Classes\Hercules" "" "Hercules HTML Document"
  WriteRegStr SHCTX "SOFTWARE\Classes\Hercules\Application" "AppUserModelId" "Hercules"
  WriteRegStr SHCTX "SOFTWARE\Classes\Hercules\Application" "ApplicationIcon" "$INSTDIR\Hercules.exe,0"
  WriteRegStr SHCTX "SOFTWARE\Classes\Hercules\Application" "ApplicationName" "Hercules"
  WriteRegStr SHCTX "SOFTWARE\Classes\Hercules\Application" "ApplicationCompany" "Hercules"      
  WriteRegStr SHCTX "SOFTWARE\Classes\Hercules\Application" "ApplicationDescription" "A privacy-focused, extensible and beautiful web browser"      
  WriteRegStr SHCTX "SOFTWARE\Classes\Hercules\DefaultIcon" "DefaultIcon" "$INSTDIR\Hercules.exe,0"
  WriteRegStr SHCTX "SOFTWARE\Classes\Hercules\shell\open\command" "" '"$INSTDIR\Hercules.exe" "%1"'

  WriteRegStr SHCTX "SOFTWARE\Classes\.htm\OpenWithProgIds" "Hercules" ""
  WriteRegStr SHCTX "SOFTWARE\Classes\.html\OpenWithProgIds" "Hercules" ""

  WriteRegStr SHCTX "SOFTWARE\Clients\StartMenuInternet\Hercules" "" "Hercules"
  WriteRegStr SHCTX "SOFTWARE\Clients\StartMenuInternet\Hercules\DefaultIcon" "" "$INSTDIR\Hercules.exe,0"
  WriteRegStr SHCTX "SOFTWARE\Clients\StartMenuInternet\Hercules\Capabilities" "ApplicationDescription" "A privacy-focused, extensible and beautiful web browser"
  WriteRegStr SHCTX "SOFTWARE\Clients\StartMenuInternet\Hercules\Capabilities" "ApplicationName" "Hercules"
  WriteRegStr SHCTX "SOFTWARE\Clients\StartMenuInternet\Hercules\Capabilities" "ApplicationIcon" "$INSTDIR\Hercules.exe,0"
  WriteRegStr SHCTX "SOFTWARE\Clients\StartMenuInternet\Hercules\Capabilities\FileAssociations" ".htm" "Hercules"
  WriteRegStr SHCTX "SOFTWARE\Clients\StartMenuInternet\Hercules\Capabilities\FileAssociations" ".html" "Hercules"
  WriteRegStr SHCTX "SOFTWARE\Clients\StartMenuInternet\Hercules\Capabilities\URLAssociations" "http" "Hercules"
  WriteRegStr SHCTX "SOFTWARE\Clients\StartMenuInternet\Hercules\Capabilities\URLAssociations" "https" "Hercules"
  WriteRegStr SHCTX "SOFTWARE\Clients\StartMenuInternet\Hercules\Capabilities\StartMenu" "StartMenuInternet" "Hercules"
  
  WriteRegDWORD SHCTX "SOFTWARE\Clients\StartMenuInternet\Hercules\InstallInfo" "IconsVisible" 1
  
  WriteRegStr SHCTX "SOFTWARE\Clients\StartMenuInternet\Hercules\shell\open\command" "" "$INSTDIR\Hercules.exe"
!macroend
!macro customUnInstall
  DeleteRegKey SHCTX "SOFTWARE\Classes\Hercules"
  DeleteRegKey SHCTX "SOFTWARE\Clients\StartMenuInternet\Hercules"
  DeleteRegValue SHCTX "SOFTWARE\RegisteredApplications" "Hercules"
!macroend